import React, {Component} from 'react';

export default class FormField extends Component {
	onClick () {
		if (typeof this.props.onClick == 'function') {
			this.props.onClick(this.props.action);
		}
	}
	render () {
		return <div className="button" onClick={this.onClick.bind(this)}>{this.props.text}</div>
	}
}
