import React, {Component} from 'react';

export default class Radio extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selected: this.props.selected || false
		};
		this.select = this.select.bind(this);
	}

	componentWillReceiveProps (newProps) {
		this.setState({
			selected: newProps.selected
		});
	}

	shouldComponentUpdate (nextProps, nextState) {
		// disabling updating radios with non-changed props
		return nextProps.selected !== this.props.selected;
	}

	select (id) {
		if (typeof this.props.select == 'function') {
			this.props.select(id);
		}
	}

	render () {
		const isSelected = this.state.selected ? 'radio_selected' : '';
		const id = this.props.id;

		const classNames = ['radio', isSelected].join(' ');

		return <div className={classNames} onClick={() => {this.select(id)}}></div>
	}
}
