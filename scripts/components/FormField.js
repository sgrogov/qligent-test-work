import React, {Component} from 'react';

import Select from './Select';
import Radio from './Radio';
import RadioGroup from './RadioGroup';

export default class FormField extends Component {
	constructor (props) {
		super(props);

		this.reset = this.reset.bind(this);
	}
	onLabelClick () {
		if (typeof this.props.onLabelClick === 'function') {
			this.props.onLabelClick();
		}
	}
	reset () {
		if (typeof this.refs.field.reset === 'function') {
			this.refs.field.reset();
		}
	}
	render () {
		const classnames = ['form-field', 'form-field_type_' + this.props.type].join(' ');

		return <div className={classnames}>
			{this.props.title ? <label className="form-field__label" onClick={this.onLabelClick.bind(this)}>{this.props.title}</label> : null}
			<div className="form-field__input">
				{(() => {
					switch (this.props.type){
						case 'select':
							return <Select ref="field" {...this.props.input} />
						case 'radio':
							return <Radio ref="field" {...this.props.input} />
						case 'radiogroup':
							return <RadioGroup ref="field" fields={this.props.fields} />
					}
				})()}
			</div>
		</div>
	}
}
