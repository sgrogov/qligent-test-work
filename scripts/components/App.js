import React, {Component} from 'react';

import Store from '../stores/ModalAssignTemplateStore';
import Modal from './Modal';

export default class App extends Component {
	constructor () {
		super()

		this.state = Store.getAll();
	}
	render() {
		const modal = this.state.modal;

		return (
			<div className="application">
				<Modal {...modal} />
			</div>
		);
	}
	componentDidMount () {
		Store.addChangeListener(() => this._onChange());
	}
	componentWillUnmount () {
		Store.removeChangeListener(() => this._onChange());
	}
	_onChange () {
		this.setState(Store.getAll());
	}
}
