import React, {Component} from 'react';

import Form from './Form';

export default class Modal extends Component {
	constructor (props) {
		super(props);

		this.state = {
			form: props.form
		};
	}
	render() {
		const form = this.state.form;

		const classNames = ['modal', this.props.opened ? 'modal_opened' : ''].join(' ');

		return <div className={classNames}>
			<div className="modal__head">
				<div className="modal-head">
					<p className="modal-head__title">Assign template</p>
					<span className="modal-head__close"></span>
				</div>
			</div>
			<div className="modal__body">
				<Form {...form} />
			</div>
		</div>
	}
}
