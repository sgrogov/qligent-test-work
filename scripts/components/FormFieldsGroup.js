import React, {Component} from 'react';

import FormField from './FormField';

export default class FormFieldsGroup extends Component {
	constructor (props) {
		super(props);

		this.reset = this.reset.bind(this);
	}
	reset () {
		for (let id in this.refs) {
			const ref = this.refs[id];
			if (typeof ref.reset === 'function') {
				ref.reset();
			}
		}
	}
	render () {
		const title = this.props.title;
		const fields = this.props.fields;

		return <div className="form-fields-group">
			<p className="form-fields-group__title">{title}</p>
			<div className="form-fields-group__body">
				{fields.map((field, i) => {
					return <FormField ref={'field' + i} key={field.id} {...field} />
				})}
			</div>
		</div>
	}
}
