import React, {Component} from 'react';

import FormFieldsGroup from './FormFieldsGroup';
import Button from './Button';

import Store from '../stores/ModalAssignTemplateStore';
import Actions from '../actions/ModalAssignTemplateActions';
import Constants from '../constants/ModalAssignTemplateConstants';

export default class Form extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form: props
		};
	}
	reset () {
		for (let id in this.refs) {
			const ref = this.refs[id];
			if (typeof ref.reset === 'function') {
				ref.reset();
			}
		}
	}
	clickButton (action) {
		switch (action) {
			case Constants.MODAL_ASSIGN_TEMPLATE_RESET:
				this.reset();
				break;
			case Constants.MODAL_ASSIGN_TEMPLATE_CLOSE:
				Actions.close();
				break;
		}
	}
	render () {
		const form = this.state.form;

		return <form className="form">
			<div className="form__body">
				{form.groups.map((group, i) => {
					return <div className="form__group">
						<FormFieldsGroup ref={'group' + i} key={i} {...group} />
					</div>
				})}
			</div>
			<div className="form__foot">
				{form.buttons.map(button => {
					return <Button {...button} onClick={this.clickButton.bind(this)} />
				})}
			</div>
		</form>
	}
}
