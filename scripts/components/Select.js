import React, {Component} from 'react';

import Actions from '../actions/ModalAssignTemplateActions';

export default class Select extends Component {
	constructor(props) {
		super(props);

		const selected = this.getSelectedValue();

		this.state = {
			opened: this.props.opened || false,
			selected: selected,
			value: selected.title
		};
		this.switch = this.switch.bind(this);

		this.reset = this.reset.bind(this);
	}
	reset () {
		this.setState({
			selected: null,
			value: null
		});
	}
	switch () {
		this.setState({
			opened: !this.state.opened
		});
	}
	select (selected) {
		this.setState({
			selected: selected,
			value: selected.title
		});
		Actions.setDirtySelect(this.props.id);
	}
	getSelectedValue () {
		let selectedValue;

		selectedValue = this.props.values.filter(value => value.selected)[0];

		if (typeof selectedValue === 'undefined') {
			selectedValue = {
				title: ''
			};
		}

		return selectedValue;
	}
	render () {
		const values = this.props.values;
		const selectedValue = this.state.value;

		const isOpened = this.state.opened ? 'select_opened' : '';

		const classnames = ['select', isOpened].join(' ');

		return <div className={classnames} onClick={this.switch}>
			<div className="select__icon select__icon_type_1"></div>
			<div className="select__value">{selectedValue}</div>
			<div className="select__arrow"></div>
			<ul className="select__list">
				{values.map((value) => {
					return <li key={value.id} className="select__item" onClick={() => {this.select(value)}}>{value.title}</li>
				})}
			</ul>
		</div>
	}
}
