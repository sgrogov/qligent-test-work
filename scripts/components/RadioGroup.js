import React, {Component} from 'react';

import FormField from './FormField';

export default class RadioGroup extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selected: this.getSelectedID()
		};
		this.reset = this.reset.bind(this);
	}
	reset () {
		this.setState({
			selected: null
		});
	}
	componentWillReceiveProps (newProps) {
		const selected = newProps.fields.filter(field => field.selected);
		if (selected.length > 0) {
			this.select(selected[0].input.id);
		}
	}
	getSelectedID () {
		let selectedID;
		try {
			selectedID = props.fields.filter(field => field.input.selected)[0].input.id;
		} catch (e) {
			selectedID = null;
		}
		return selectedID;
	}
	select (selectedID) {
		this.setState({selected: selectedID});
	}

	render () {
		const radios = this.props.fields.map((field, i) => {
			field.input.select = this.select.bind(this);
			field.input.selected = field.input.id === this.state.selected;

			field.onLabelClick = () => {
				this.select(field.input.id);
			}

			return <FormField ref={'field' + i} key={field.id} {...field} />
		});

		return <div className="radiogroup">{radios}</div>;
	}
}
