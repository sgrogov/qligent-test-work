import {EventEmitter} from 'events';
import assign from 'object-assign';

import AppDispatcher from '../dispatcher/AppDispatcher';
import Constants from '../constants/ModalAssignTemplateConstants';

const dirtySelects = new Set();

const CHANGE_EVENT = 'CHANGE_EVENT';

const RADIOGROUP = [
	{
		"id": 0,
		"title": "Assign conditionals",
		"type": "radio",
		"input": {
			"id": Constants.MODAL_ASSIGN_TEMPLATE_ONLY_CONDITIONALS
		}
	}, {
		"id": 1,
		"title": "Assign notifications",
		"type": "radio",
		"input": {
			"id": Constants.MODAL_ASSIGN_TEMPLATE_ONLY_NOTIFICATIONS
		}
	}, {
		"id": 2,
		"title": "Assign both",
		"type": "radio",
		"input": {
			"id": Constants.MODAL_ASSIGN_TEMPLATE_BOTH
		}
	}
];

const data = {
	"modal": {
		"opened": true,
		"form": {
			"groups": [
				{
					"id": 0,
					"title": "Select template",
					"fields": [
						{
							"id": 0,
							"title": "Conditions",
							"type": "select",
							"input": {
								"id": Constants.MODAL_ASSIGN_TEMPLATE_TYPE_CONDITIONS,
								"values": [
									{
										"id": 0,
										"title": "Cndtn Tmplt 1"
									}, {
										"id": 1,
										"title": "Cndtn Tmplt 2"
									}, {
										"id": 2,
										"title": "Cndtn Tmplt 3"
									}, {
										"id": 3,
										"title": "Cndtn Tmplt 4"
									}
								]
							}
						}, {
							"id": 1,
							"title": "Notifications",
							"type": "select",
							"input": {
								"id": Constants.MODAL_ASSIGN_TEMPLATE_TYPE_NOTIFICATIONS,
								"values": [
									{
										"id": 0,
										"title": "Ntfctn Tmplt 1"
									}, {
										"id": 1,
										"title": "Ntfctn Tmplt 2"
									}, {
										"id": 2,
										"title": "Ntfctn Tmplt 3"
									}, {
										"id": 3,
										"title": "Ntfctn Tmplt 4"
									}
								]
							}
						}
					],
				}, {
					"id": 1,
					"title": "Assign template type",
					"fields": [
						{
							"id": 2,
							"type": "radiogroup",
							"fields": RADIOGROUP
						}
					]
				}
			],
			"buttons": [
				{
					"text": "Assign Template",
					"action": Constants.MODAL_ASSIGN_TEMPLATE_RESET
				}, {
					"text": "Cancel",
					"action": Constants.MODAL_ASSIGN_TEMPLATE_CLOSE
				}
			]
		}
	}
};

const ModalAssignTemplateStore = assign({}, EventEmitter.prototype, {
	close () {
		data.modal.opened = false;
	},
	selectType (id) {
		RADIOGROUP.forEach(group => {
			if (group.input.id == id) {
				group.selected = true;
			} else {
				group.selected = false;
			}
		});
	},
	setDirtySelect (id) {
		const conditions = Constants.MODAL_ASSIGN_TEMPLATE_TYPE_CONDITIONS;
		const notifications = Constants.MODAL_ASSIGN_TEMPLATE_TYPE_NOTIFICATIONS;

		dirtySelects.add(id);

		if (dirtySelects.has(conditions) && dirtySelects.has(notifications)) {
			this.selectType(Constants.MODAL_ASSIGN_TEMPLATE_BOTH);
		} else if (dirtySelects.has(conditions)) {
			this.selectType(Constants.MODAL_ASSIGN_TEMPLATE_ONLY_CONDITIONALS);
		} else if (dirtySelects.has(notifications)) {
			this.selectType(Constants.MODAL_ASSIGN_TEMPLATE_ONLY_NOTIFICATIONS);
		}
	},
	getAll() {
		return data;
	},
	addChangeListener (callback) {
		this.on(CHANGE_EVENT, callback);
	},
	emitChange () {
		this.emit(CHANGE_EVENT);
	}
});


AppDispatcher.register(function(action) {
	switch(action.actionType) {
		case Constants.ACTION_SET_DIRTY_SELECT:
			ModalAssignTemplateStore.setDirtySelect(action.id);
			ModalAssignTemplateStore.emitChange();
			break;
		case Constants.MODAL_ASSIGN_TEMPLATES_CLOSE:
			ModalAssignTemplateStore.close();
			ModalAssignTemplateStore.emitChange();
			break;
	}
});

export default ModalAssignTemplateStore;
