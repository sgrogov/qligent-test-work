import AppDispatcher from '../dispatcher/AppDispatcher';

import Constants from '../constants/ModalAssignTemplateConstants';

const ModalAssignTemplateActions = {
	setDirtySelect (id) {
		AppDispatcher.dispatch({
			actionType: Constants.ACTION_SET_DIRTY_SELECT,
			id: id
		});
	},
	reset () {
		AppDispatcher.dispatch({
			actionType: Constants.MODAL_ASSIGN_TEMPLATES_RESET
		});
	},
	close () {
		AppDispatcher.dispatch({
			actionType: Constants.MODAL_ASSIGN_TEMPLATES_CLOSE
		});
	}
}

export default ModalAssignTemplateActions;
