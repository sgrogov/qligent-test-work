# Тестовое задание для Qligent

Разработано на основе https://github.com/vasanthk/react-es6-webpack-boilerplate.
Компоненты приложения расположены в `/app/components/`.
Все используемые данные забиты хардкодом внутри `/app/stores/ModalAssignTemplateStore.js`

### Запуск

```
npm install
npm start
Open http://localhost:5000
```
